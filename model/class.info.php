<?php

class Info
{
 public $conn;
 public $sql;

 public function __construct(){
   $db = new DatabaseConnection;
   $this->conn = $db->connectDB();
   $this->sql = $db;
 }


 public function newSubmit($args){
   $post = $this->sql->sqlHandler($args);
   return $post;
 }


 /**
 * Get Media Collections
 * @param $args : array of values for query
 */
 public function getAllInfo($args){
   $post = $this->sql->sqlHandler($args);

   //Get media collection name based on Media Collection ID
   foreach($post as $i=>$p){
     $args2 = array(
       'args'=>array('id'=>(int)$p['info_type'],'info_type'=>'','active'=>1,'app_only'=>''),
       'action'=>'select',
       'target'=>array('id'),
       'table'=>'info_types',
       'file'=>''
     );

     $infoTypeName = $this->getInfoTypeName($args2);

     $post[$i]['info_type'] = $infoTypeName[0]['info_type'];
   }
   return $post;
 }

}
?>
