<?php

class Imagery
{
	public $conn;
	public $hostPath;

	public function __construct(){
		$url = $_SERVER['DOCUMENT_ROOT'];
		if(strpos($url, 'proposal') !== false){
			$this->hostPath = $_SERVER['DOCUMENT_ROOT'];
		}else{
			$this->hostPath = $_SERVER['DOCUMENT_ROOT']."/arup-idea";
		}
	}

	public function generateRandomString($length = 20) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
	}

  public function imageHandler($_FILE){

		$imgFile = $_FILE['image']['name'];
		$tmp_dir = $_FILE['image']['tmp_name'];
		$imgSize = $_FILE['image']['size'];
		$imgType = $_FILE['image']['type'];

		//Folder path placement/generation
		if(count(glob("upload/*"))==0){
			$randomString = $this->generateRandomString();
			//var_dump($this->hostPath);

			mkdir("".$this->hostPath."/upload/" . $randomString . "/", 0777);
			$upload_dir = "".$this->hostPath."/upload/".$randomString."/";
			$upload_dir_text = "/upload/".$randomString."/";

		}else{
			foreach (glob("upload/*") as $dir){
				if (count(glob("upload/".$dir."/*")) < 50){
					if($dir!="timelines"&&$dir!="signatures"){
						//fine to put image in here

						$upload_dir = "".$this->hostPath."/".$dir."/";
						$upload_dir_text = "/".$dir."/";
					}
				}elseif(count(glob("upload/".$dir."/*")) == 50){
					if($dir!="timelines"&&$dir!="signatures"){
						//new folder needed
						$randomString = $this->generateRandomString();
						$checkDir = "".$this->hostPath."/upload/" . $randomString . "/";

						$folderSelected=0;
						while($folderSelected!=1){
							if (!file_exists($checkDir)) {
								mkdir("".$this->hostPath."/upload/" . $randomString . "/", 0777);
									$upload_dir = "".$this->hostPath."/upload/".$randomString."/";
									$upload_dir_text = "/upload/".$randomString."/";

									$folderSelected=1;
							}
						}
					}
				}		
			}
		}

		$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

		$userpic = rand(1000,1000000).".".$imgExt;
		$this->makeThumbnail($tmp_dir,1400, 1400, $upload_dir.$userpic, $imgType);

		return  $upload_dir_text.$userpic;
	}
	public function makeThumbnail($sourcefile,$max_width, $max_height, $endfile, $type){
		switch($type){
			case "image/png":
				move_uploaded_file($sourcefile,$endfile);
				$sourcefile = $endfile;
				$img = imagecreatefrompng($sourcefile);
			break;
			case "image/jpeg":
				move_uploaded_file($sourcefile,$endfile);
				$sourcefile = $endfile;
				$img = imagecreatefromjpeg($sourcefile);
			break;
			case "image/jpg":
				move_uploaded_file($sourcefile,$endfile);
				$sourcefile = $endfile;
				$img = imagecreatefromjpeg($sourcefile);
			break;
			case "image/gif":
				$img = imagecreatefromgif($sourcefile);
			break;
		}

		$width = imagesx($img);
		$height = imagesy($img);

		if($width > $height){
				$newwidth = $max_width;
				$divisor = $width / $newwidth;
				$newheight = floor( $height / $divisor);
		}else{
				$newheight = $max_height;
				$divisor = $height / $newheight;
				$newwidth = floor( $width / $divisor);
		}

		$tmpimg = imagecreatetruecolor($newwidth, $newheight);

		imagealphablending($tmpimg, false);
		imagesavealpha($tmpimg, true);

		imagecopyresampled($tmpimg, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		switch($type){
			case "image/png":
				imagepng($tmpimg, $endfile, 1);
			break;
			case "image/jpeg":
				imagejpeg($tmpimg, $endfile, 75);
			break;
			case "image/jpg":
				imagejpeg($tmpimg, $endfile, 75);
			break;
			case "image/gif":
				imagegif($tmpimg, $endfile, 75);
			break;
		}

		imagedestroy($tmpimg);
		imagedestroy($img);

	}

}
?>
