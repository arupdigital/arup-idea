<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer">
  <div class="mdl-layout__drawer">
    <img src="<?php echo SITEPATH ?>/assets/img/arupidea.jpg" alt="" id="navlogo">
    <nav class="mdl-navigation">
      <?php
      foreach($GLOBALS['nav'] as $navItem) {
        if($GLOBALS['active']==$navItem['name']){
          echo '<a class="active mdl-navigation__link" href="'. SITEPATH .'/'. $navItem['url'].'/">'.$navItem['name'].'</a>';
        }else{
          echo '<a class="mdl-navigation__link" href="'. SITEPATH .'/'. $navItem['url'].'/">'.$navItem['name'].'</a>';
        }
      }
      ?>
    </nav>
  </div>
</div>