$(document).ready(function(){
    $(document).on('change', '#switch-1', toggleProjType);

    $(document).on('click', '#showRows', toggleRowCards);
    $(document).on('click', '#showCardss', toggleRowCards);

    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 20000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
});

function toggleProjType(e) {
    const tog = e.target;

    if($(tog).prop("checked")) {
        $(tog).attr('checked', true);
        $('#existing').css('display', 'none');
        $('#new').css('display', 'block');
    }else {
        $(tog).attr('checked', false);
        $('#existing').css('display', 'block');
        $('#new').css('display', 'none');
    }
}

function toggleRowCards(e) {
    let tog = e.target;
    tog = $(tog).parent();

    if($(tog).hasClass("toggled") && $(tog).attr('id') == 'showRows') { // show rows currently active
        $('#showRows').removeClass("toggled");
        $('#showRows').html('List View<span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span>');
        $('#cards').css('display', 'block');
        $('#list').css('display', 'none');
    }else if(!$(tog).hasClass("toggled") && $(tog).attr('id') == 'showRows') { // show rows NOT currently active
        $('#showRows').addClass("toggled");
        $('#showRows').html('Card View<span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span>');
        $('#cards').css('display', 'none');
        $('#list').css('display', 'block');
    }
}