<?php
$state = 0;
    if(isset($_POST['action'])){
        unset($_POST['action']);

        $post = array(
            'id'=>(int)$id, 
        );
        $_POST['updated_count'] = $_POST['updated_count'] + 1;

        /* remove flag values */
        unset($_POST['action']);
        unset($_POST['submit']);

        /* toggle handling */
        if(!isset($_POST['existing_project'])){
            $_POST['existing_project'] = 0;
        }else{
            $_POST['existing_project'] = 1;
        }

        if(!isset($_POST['past_client'])){
            $_POST['past_client'] = 0;
        }else{
            $_POST['past_client'] = 1;
        }

        if(!isset($_POST['display_anon'])){
            $_POST['display_anon'] = 0;
        }else{
            $_POST['display_anon'] = 1;
        }
        /* end toggle handling */

        foreach($_POST as $a=>$b){
            (is_string($b) ? $b = "$b" : $b );
            $post[$a] = $b; 
        }
        $img = $_FILES['image'];
        $image = array('image'=>'');
        $image['image'] = $img;
        $_POST['image'] = '';

        $args = array(
            'args'=>$post,
            'action'=>'update',
            'target'=>array('id'),
            'table'=>'ideabank',
            'file'=>$image
        );

        $info = new Info;
        $res = $info->newSubmit($args);
        $state = 1;
    }

    $args = array(
        'args'=>array('id'=>(int)$id, '*'),
        'action'=>'select',
        'target'=>array('id'),
        'table'=>'ideabank',
        'file'=>''
    );

    $info = new Info;
    $result = $info->newSubmit($args);

?>