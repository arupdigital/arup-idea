<?php

$state = 0;

if(isset($_POST['action'])){
    
    /* remove flag values */
    unset($_POST['action']);
    unset($_POST['submit']);

    /* toggle handling */
    if(!isset($_POST['existing_project'])){
        $_POST['existing_project'] = 0;
    }else{
        $_POST['existing_project'] = 1;
    }

    if(!isset($_POST['past_client'])){
        $_POST['past_client'] = 0;
    }else{
        $_POST['past_client'] = 1;
    }

    if(!isset($_POST['display_anon'])){
        $_POST['display_anon'] = 0;
    }else{
        $_POST['display_anon'] = 1;
    }

    $_POST['author'] = $_SESSION["email"];
    /* end toggle handling */

    $date = new DateTime();
    $_POST['date_created'] = $date->format('Y-m-d H:i:sP');
    $_POST['active'] = 1;
    $_POST['updated_count'] = 0;

    $img = $_FILES['image'];
    $image = array('image'=>'');
    $image['image'] = $img;
    $_POST['image'] = '';

    $obj = $_POST;

    $args = array(
        'args'=>$obj,
        'action'=>'insert',
        'target'=>'',
        'table'=>'ideabank',
        'file'=>$image
    );

    $info = new Info;
    $res = $info->newSubmit($args);

    $state = 1;

}

?>