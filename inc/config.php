<?php
date_default_timezone_set('America/Toronto');

//Section class_parent
	define("APP","app");

//Site path defined
	$sitePath = 'http://' . $_SERVER['HTTP_HOST'] . "/arup-idea";
	define("SITEPATH", $sitePath);

	echo "<script>
	var SITEPATH = '".SITEPATH."';
	</script>";

//Navigation Map
	$nav = array();
	$navSite = array();
	$navSite[] = array("name"=>"Concept","url"=>"concept","icon"=>"","type"=>"");
	$navSite[] = array("name"=>"IDEA Wall","url"=>"idea-wall","icon"=>"","type"=>"");
	$navSite[] = array("name"=>"Submit","url"=>"submit","icon"=>"","type"=>"");
	$navSite[] = array("name"=>"Logout","url"=>"logout","icon"=>"","type"=>"");
	//$navSite[] = array("name"=>"Settings","url"=>"settings","icon"=>"glyphicon-cog","type"=>"");
	/*$navSite[] = array("name"=>"Admin Area","url"=>"admin","icon"=>"glyphicon-tasks","type"=>"");
	$navSite[] = array("name"=>"".$_SESSION["user"]."","url"=>"user/","icon"=>"glyphicon-user","type"=>"");
	$navSite[] = array("name"=>"Browse","url"=>"requirements","icon"=>"glyphicon glyphicon-th-list","type"=>"title");*/
	//$navSite[] = array("name"=>"About","url"=>"home","icon"=>"glyphicon glyphicon-home","type"=>"");


	//Final nav object
	$GLOBALS['nav'] = $navSite;

//DATA
	$dataSet = array(
		array("field"=>"id","displayName"=>"ID"),
		array("field"=>"name","displayName"=>"Name"),
		array("field"=>"existing_project","displayName"=>"Existing Project"),
		array("field"=>"project_ref","displayName"=>"Project Reference"),
		array("field"=>"client","displayName"=>"Client"),
		array("field"=>"past_client","displayName"=>"Past Client"),
		array("field"=>"descript","displayName"=>"Description"),
		array("field"=>"potential_buyers","displayName"=>"Market Potential"),
		array("field"=>"un_sgd","displayName"=>"UN SDG(s)"),
		array("field"=>"date_created","displayName"=>"Date Created"),
		array("field"=>"active","displayName"=>"active"),
		array("field"=>"updated_count","displayName"=>"Updated Count"),
		array("field"=>"talent_mentors","displayName"=>"Talent Mentors"),
		array("field"=>"market_opportunity","displayName"=>"Market Opportunity"),
		array("field"=>"display_anon","displayName"=>"Anonymous"),
	);
	$GLOBALS['data'] = $dataSet;

//Routing Init
	include("model/class.route.php");
	$route = new Route();
	$u = $_REQUEST['uri'];

	//Final active page
	$GLOBALS['active'] = $u;

	//Route Map
	include("appRoute.php");
?>
