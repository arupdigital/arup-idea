<?php
    include('./controller/controller.edit.php');
?>


<section id='submitpage'>
    <h1 class='header'>The Arup IDEA Checklist</h1>

    <?php
        if($state==1){
            if($res!=""){
                $msg = "updated!";
            }else{
                $msg = "not updated!";
            }
            echo "Hey guess what! Your submission was ". $msg;
        }else{
    ?>
    <form action="" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="submit" />
        <input type="hidden" name="updated_count" value="<?php echo $result[0]['updated_count']; ?>" />

        <div class="mdl-textfield mdl-js-textfield">
            Name:
            <input class="mdl-textfield__input" type="text" id="name" name="name" value="<?php echo $result[0]['name']; ?>">
            <label class="mdl-textfield__label" for="name"></label>
        </div>

        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-1">
            <input type="checkbox" id="switch-1" class="mdl-switch__input" name="existing_project" 
                <?php if( $result[0]['existing_project'] == 1 ){ echo "checked";} ?>
            >
            <span class="mdl-switch__label">Current Project / New Project</span>
        </label>

        <div id="project-switch">
            <div id="existing">
                <div class="mdl-textfield mdl-js-textfield">
                    Which project?
                    <input class="mdl-textfield__input" type="text" id="project1" name="project_ref" value="<?php echo $result[0]['project_ref']; ?>">
                    <label class="mdl-textfield__label" for="project"></label>
                </div>
            </div>
            <div id="new">
                <div class="mdl-textfield mdl-js-textfield">
                    Who might the client be?
                    <input class="mdl-textfield__input" type="text" id="project2" name="client" value="<?php echo $result[0]['client']; ?>">
                    <label class="mdl-textfield__label" for="project"></label>
                </div>
                <br/>
                Have we worked with this client before?<br/>
                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-2">
                    <input type="checkbox" id="switch-2" class="mdl-switch__input" name="past_client"
                        <?php if( $result[0]['past_client'] == 1 ){ echo "checked";} ?>
                    >
                    <span class="mdl-switch__label">No / Yes</span>
                </label>
            </div>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield-textarea">
            Describe the IDEA:
            <textarea class="mdl-textfield__input" type="text" rows= "8" id="sample5" name="descript" ><?php echo $result[0]['descript']; ?></textarea>
            <label class="mdl-textfield__label" for="sample5"></label>
        </div>
        <br/>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield-textarea">
            Who might we sell this IDEA to?
            <textarea class="mdl-textfield__input" type="text" rows= "8" id="buyers" name="potential_buyers" ><?php echo $result[0]['potential_buyers']; ?></textarea>
            <label class="mdl-textfield__label" for="buyers"></label>
        </div>
        <br/> 
        <div class="mdl-textfield mdl-js-textfield">
            Describe how the IDEA may support the <strong>UN SDGs</strong> and identify up to three relevant SDGs
            <input class="mdl-textfield__input" type="text" id="unsgd" name="un_sgd" value="<?php echo $result[0]['un_sgd']; ?>">
            <label class="mdl-textfield__label" for="name"></label>
            <p><a href="
            https://arup.sharepoint.com/teams/180308a/Shared%20Documents/SDG's%20Pocket%20Guide_5.2_Digital.pdf#search=un%20sdg">Click here to view the UN SDGs</a></p>
        </div>

        <div class="flex-splitter">
            <img src="<?php echo SITEPATH ?>/assets/img/bulb.jpg" alt="">
            <!--<p>
                When you're done, post your IDEA on the IDEA Wall! You can come back and make changes any time.
            </p>-->
        </div>
        <br/>
        Do you prefer to be anonymous?<br/>
        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-3">
            <input type="checkbox" id="switch-3" class="mdl-switch__input" name="display_anon"
                <?php if( $result[0]['display_anon'] == 1 ){ echo "checked";} ?>
            >
            <span class="mdl-switch__label">No / Yes</span>
        </label>
        <br/>
        <br/>
        Upload an image<br/>
        <input type='file' name="image" onchange="readURL(this);" accept='image/*' />
        <?php
            if($result[0]['image']!="") {
                $img = ''.SITEPATH.$result[0]['image'].'';
                $style = 'style="display:block;"';
            }else{
                $img = '';
                $style = 'style="display:none;"';
            }
        ?>
        <img id="myimg" src="<?php echo $img; ?>" <?php echo $style; ?> />
        <br/><br/>
        <button name="submit" value="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
            Submit
        </button>

        <script>
        var img = document.getElementById('myimg'); 
        //or however you get a handle to the IMG
        var width = img.clientWidth;
        var height = img.clientHeight;
        var size = calculateAspectRatioFit(width, height, 200, 200)
        img.width = size.width;
        img.height = size.height;

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = (function (e) {
                    var image = new Image();
                    image.src = e.target.result;

                    image.onload = function() {
                        console.log(this.width, this.height);

                        var size = calculateAspectRatioFit(this.width, this.height, 200, 200)

                        $('#myimg')
                            .attr('src', this.src)
                            .width(size.width)
                            .height(size.height);
                        
                        $('#myimg').css('display','block');
                    }
                });

                reader.readAsDataURL(input.files[0]);
            }
        }
        function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
            var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
            return { width: srcWidth*ratio, height: srcHeight*ratio };
        }
        </script>

        <h1 class="header2"></h1>

        <p style="font-size: 20px;padding: 20px 0;text-align: center;">The Core IDEA will pair winning IDEA holders with one or more discipline specialists or mentors in the office to help develop the IDEA and identify marketing opportunities.</p>

        <!--<div class="mdl-textfield mdl-js-textfield">
            Discipline specialist(s)/mentor(s)
            <input class="mdl-textfield__input" type="text" id="talent" name="talent_mentors" value="">
            <label class="mdl-textfield__label" for="name"></label>
        </div>
        <br/>
        <div class="mdl-textfield mdl-js-textfield">
            Marketing opportunities
            <input class="mdl-textfield__input" type="text" id="market" name="market_opportunity" value="">
            <label class="mdl-textfield__label" for="name"></label>
        </div>-->
    </form>
    <?php
        } 
    ?>
</section>
