<?php
    include('./controller/controller.entires.php');
?>

<section id="idea-wall">
  <h1 class='header'>IDEA Entries 
    <div class="editIdea" style="top: 0px;right: 0px;">
      <button id="showRows" class='mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored toggleIcon'>
        List View
      </button>
    </div>
  </h1>

  <div id="list" class="swiper-container">
    <table>
      <?php
      $c=0;
      $firstRow="<tr>";
      foreach($res as $k=>$r){
        $id=$r['id'];
        $author=$r['author'];
        unset($r['id']);
        unset($r['author']);
        unset($r['active']);
        unset($r['updated_count']);

        echo "<tr>";
        $cell=0;
        unset($r["image"]);

        // display name if not anonymous
        if($r["display_anon"] == 1){
          $r['name'] = "Anonymous";
        }
        foreach($r as $h=>$v){
          foreach($GLOBALS['data'] as $data1 => $data2){

            if($h == $data2['field']){
              $h = $data2['displayName'];

              // boolean look
              if(is_int($v)){
                if($v==1){
                  $v = "Yes";
                }else{
                  $v = "No";
                }
              }

              if($data2['field']=="date_created"){
                $v = explode(' ', $v);
                $v = $v[0];
              }
            }
          }
          if($c==0){
            if($cell==0){
              echo "<th class='list- list-".makeSEOURL($h)."'>Edit</th>";
            }
            echo "<th class='list- list-".makeSEOURL($h)."'>".$h."</th>";

            if($cell==0){
              $firstRow .= "<td class='list-'>";

                if($author == $_SESSION["email"]) {
                  $firstRow .="<a href='".SITEPATH."/idea-wall/edit/".$id."'>
                  <button class='mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored'>
                    EDIT
                  </button>
                  </a>";
                }

              $firstRow .="</td>";
            }
            
            $firstRow .= "<td class='list- list-".makeSEOURL($h)."'>".$v."</td>";

          }else{
            if($cell==0){
              echo "<td class='list-'>";
              
              if($author == $_SESSION["email"]) {
                echo "<a href='".SITEPATH."/idea-wall/edit/".$id."'>
                <button class='mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored'>
                  EDIT
                </button>
                </a>";
              }

              echo "</td>";
            }
              echo "<td class='list- list-".makeSEOURL($h)."'>".$v."</td>";
          }
          $cell=1;
        }
        echo "</tr>";

        if($c==0){
          $firstRow .= "</tr>";
          echo $firstRow;
        }
        $c++;
      }
      ?>
    </table>
  </div>

  <div id="cards" class="swiper-container">
    <div class="swiper-wrapper">
      <?php
        foreach($res as $r){
          $author=$r['author'];
          unset($r['author']);
          if ($r['image']!='') {
            $imgBK = "background: linear-gradient(to left, rgba(191, 104, 101, 0.9) 0%, rgba(191, 104, 101, .9) 100%), url(". SITEPATH . $r['image'].") 50% 50% / cover no-repeat;";
          }else{
            $imgBK = '';
          }
          unset($r['image']);

          echo "<div class='swiper-slide entry-slide' style='".$imgBK."'>
          <div class='slideNumber'>ID #".$r['id']."</div>

          ";

          if($author == $_SESSION["email"]) {
            echo "
            <a href='".SITEPATH."/idea-wall/edit/".$r['id']."'>
              <button class='mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored editIdea'>
                EDIT
              </button>
            </a>";
          }
          
          echo"
          <table>";
            unset($r['id']);

            // unsets project or client
            if($r["existing_project"] == 0){
              unset($r['client']);
              unset($r['past_client']);
            }else{
              unset($r['project_ref']);
            }
            unset($r["existing_project"]);

            // display name if not anonymous
            if($r["display_anon"] == 1){
              $r['name'] = "Anonymous";
            }

            // unset active and updated count
            unset($r['active']);
            unset($r['updated_count']);
          foreach($r as $k => $v){

            // data correction for display
            foreach($GLOBALS['data'] as $data1 => $data2){
              if($k == $data2['field']){
                $k = $data2['displayName'];

                // boolean look
                if(is_int($v)){
                  if($v==1){
                    $v = "Yes";
                  }else{
                    $v = "No";
                  }
                }

                if($data2['field']=="date_created"){
                  $v = explode(' ', $v);
                  $v = $v[0];
                }
              }
            }
            
            if($v != ""){
              echo '<tr><td class="table-param"><strong>' . $k . '</strong></td><td class="table-val">' . $v . "</td></tr>";
            }
          }
          echo "</table>
          </div>";
        }
      ?>
    </div>
    <div class="swiper-pagination"></div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>

</section>
