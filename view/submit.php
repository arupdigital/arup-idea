<?php
    include('./controller/controller.submit.php');
?>

<section id='submitpage'>
    <h1 class='header'>The Arup IDEA Checklist</h1>

    <?php
        if($state==1){
            if($res==1){
                $msg = "was successful!";
            }else{
                $msg = "had failed!";
            }
            echo "Hey guess what! Your submission ". $msg;
        }else{
    ?>
    <form action="<?php echo SITEPATH; ?>/submit" method="post" enctype="multipart/form-data" >
        <input type="hidden" name="action" value="submit" />

        <div class="mdl-textfield mdl-js-textfield">
            Name:
            <input class="mdl-textfield__input" type="text" id="name" name="name" value="">
            <label class="mdl-textfield__label" for="name"></label>
        </div>

        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-1">
            <input type="checkbox" id="switch-1" class="mdl-switch__input" name="existing_project">
            <span class="mdl-switch__label">Current Project / New Project</span>
        </label>

        <div id="project-switch">
            <div id="existing">
                <div class="mdl-textfield mdl-js-textfield">
                    Which project?
                    <input class="mdl-textfield__input" type="text" id="project1" name="project_ref" value="">
                    <label class="mdl-textfield__label" for="project"></label>
                </div>
            </div>
            <div id="new">
                <div class="mdl-textfield mdl-js-textfield">
                    Who might the client be?
                    <input class="mdl-textfield__input" type="text" id="project2" name="client" value="">
                    <label class="mdl-textfield__label" for="project"></label>
                </div>
                <br/>
                Have we worked with this client before?<br/>
                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-2">
                    <input type="checkbox" id="switch-2" class="mdl-switch__input" name="past_client" checked>
                    <span class="mdl-switch__label">No / Yes</span>
                </label>
            </div>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield-textarea">
            Describe the IDEA:
            <textarea class="mdl-textfield__input" type="text" rows= "8" id="sample5" name="descript" ></textarea>
            <label class="mdl-textfield__label" for="sample5"></label>
        </div>
        <br/>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield-textarea">
            Who might we sell this IDEA to?
            <textarea class="mdl-textfield__input" type="text" rows= "8" id="buyers" name="potential_buyers" ></textarea>
            <label class="mdl-textfield__label" for="buyers"></label>
        </div>
        <br/> 
        <div class="mdl-textfield mdl-js-textfield">
            Describe how the IDEA may support the <strong>UN SDGs</strong> and identify up to three relevant SDGs
            <input class="mdl-textfield__input" type="text" id="unsgd" name="un_sgd" value="">
            <label class="mdl-textfield__label" for="name"></label>
            <p><a href="
            https://arup.sharepoint.com/teams/180308a/Shared%20Documents/SDG's%20Pocket%20Guide_5.2_Digital.pdf#search=un%20sdg">Click here to view the UN SDGs</a></p>
        </div>


        <br/>
        Do you prefer to be anonymous?<br/>
        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-3">
            <input type="checkbox" id="switch-3" class="mdl-switch__input" name="display_anon">
            <span class="mdl-switch__label">No / Yes</span>
        </label>
        <br/>
        <br/>
        Upload an image<br/>
        <input type='file' name="image" onchange="readURL(this);" accept='image/*' />
        <img id="myimg" src="#" style="display:none;" />
        <br/><br/>
        <button name="submit" value="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
            Submit
        </button>

        <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = (function (e) {
                    var image = new Image();
                    image.src = e.target.result;

                    image.onload = function() {
                        console.log(this.width, this.height);

                        var size = calculateAspectRatioFit(this.width, this.height, 200, 200)

                        $('#myimg')
                            .attr('src', this.src)
                            .width(size.width)
                            .height(size.height);
                        
                        $('#myimg').css('display','block');
                    }
                });

                reader.readAsDataURL(input.files[0]);
            }
        }
        function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
            var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
            return { width: srcWidth*ratio, height: srcHeight*ratio };
        }
        </script>

        <h1 class="header2"></h1>

        <p style="font-size: 20px;padding: 20px 0;text-align: center;">The Core IDEA will pair winning IDEA holders with one or more discipline specialists or mentors in the office to help develop the IDEA and identify marketing opportunities.</p>

        <!--<div class="mdl-textfield mdl-js-textfield">
            Discipline specialist(s)/mentor(s)
            <input class="mdl-textfield__input" type="text" id="talent" name="talent_mentors" value="">
            <label class="mdl-textfield__label" for="name"></label>
        </div>
        <br/>
        <div class="mdl-textfield mdl-js-textfield">
            Marketing opportunities
            <input class="mdl-textfield__input" type="text" id="market" name="market_opportunity" value="">
            <label class="mdl-textfield__label" for="name"></label>
        </div>-->
    </form>
    <?php
        } 
    ?>
</section>